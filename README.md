# vlbeaudoin/tmux

Installer par symlink la config tmux.

Utilise ansible pour installer la config et le package, alors devrait être assez facile d'ajuster les fichiers pour déployer à distance.

## Repo

[vlbeaudoin/tmux](https://git.agecem.com/vlbeaudoin/tmux)

## Fichiers à vérifier

### vars.yml

Variables pour ansible playbook `deploy.yml`

### inventory.yml

Inventaire ansible

## Fichiers exécuter

### deploy.sh

Installer tmux et linker configs depuis ce repo vers `~/.tmux.conf`.

## Procédure

```
# Voir aide de Makefile
make help

# Linker config tmux à ~/.tmux.conf
make deploy
```

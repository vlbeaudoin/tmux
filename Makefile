# SHELL = /bin/sh

.DEFAULT_GOAL := help

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: deploy
deploy: ## Retirer la config tmux (si présente) et symlink la config depuis files/tmux.conf
	@rm -f -v '$(HOME)/.tmux.conf'
	@ln -s -v '$(shell pwd)/files/tmux.conf' '$(HOME)/.tmux.conf'
